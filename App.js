import React, { useState } from 'react';
import { Platform, StatusBar, View } from 'react-native';
import { AppearanceProvider } from 'react-native-appearance';
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import { Provider } from 'react-redux';
import rootReducer from './src/vb_data_layer/reducers';
import configureStore from './src/store/configureStore';
import AppNavigator from './src/navigation/AppNavigator';

console.disableYellowBox = true;

const backgroundImageDark = require('./assets/vbBackgroundDark.png');
const arrivalPic = require('./assets/vbImages/arrival.png');
const calendarPic = require('./assets/vbImages/calendar.png');
const currencyPic = require('./assets/vbImages/currencySign.png');
const departurePic = require('./assets/vbImages/departure.png');
const passengerPic = require('./assets/vbImages/passenger.png');
const planeLarge = require('./assets/vbImages/plane.png');
const smallPlane = require('./assets/vbImages/smallPlane.png');
const chevronLeft = require('./assets/vbImages/chevronLeft.png');
const infant = require('./assets/vbImages/infant.png');
const adult = require('./assets/vbImages/adult.png');
const child = require('./assets/vbImages/child.png');
const arrowRight = require('./assets/vbImages/arrowRight.png');
const vivaFare = require('./assets/vbImages/vivaFare.png');
const vivaSuperFare = require('./assets/vbImages/vivaSuperFare.png');
const vivaMaxFare = require('./assets/vbImages/vivaMaxFare.png');

const store = configureStore(rootReducer, {});

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      backgroundImageDark,
      arrivalPic,
      calendarPic,
      currencyPic,
      departurePic,
      passengerPic,
      planeLarge,
      smallPlane,
      chevronLeft,
      infant,
      adult,
      child,
      arrowRight,
      vivaFare,
      vivaSuperFare,
      vivaMaxFare
    ])
  ]);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

function handleLoadingError(error) {
  console.warn(error);
}

export default function App() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  if (!isLoadingComplete) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  }
  return (
    <AppearanceProvider>
      <Provider store={store}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#fff',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          {Platform.OS === 'ios' && <StatusBar barStyle="light-content" />}
          <AppNavigator />
        </View>
      </Provider>
    </AppearanceProvider>
  );
}
