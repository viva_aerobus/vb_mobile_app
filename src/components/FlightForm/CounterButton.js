import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import noop from 'lodash/noop';
import { string, func, number, node } from 'prop-types';

import styles from './styles';

const {
  pOFormFieldContainer,
  formFieldLabelWrapper,
  countButtonInputWrapper,
  formLabelTextSize,
  darkGrayText,
  counterActionable,
  currentCountWrapper,
  labelTitleWrapper,
  backButton,
  lableTextWrapper,
  labelComplementWrapper,
  labelComplementTextSize,
  counterButtonLabelText,
  counterControlsText
} = styles;

const CounterButton = props => (
  <View style={pOFormFieldContainer}>
    <View style={formFieldLabelWrapper}>
      <View style={labelTitleWrapper}>
        <Image style={backButton} source={props.labelIcon} />
        <View style={lableTextWrapper}>
          <Text style={counterButtonLabelText}>{props.label}</Text>
        </View>
      </View>
      <View style={labelComplementWrapper}>
        <Text style={[labelComplementTextSize, darkGrayText]}>
          {props.labelComplement}
        </Text>
      </View>
    </View>
    <View style={countButtonInputWrapper}>
      <TouchableOpacity
        style={counterActionable}
        onPress={() => props.modifyCounter(props.formField, '-')}
      >
        <Text style={counterControlsText}>-</Text>
      </TouchableOpacity>
      <View style={currentCountWrapper}>
        <Text style={counterControlsText}>{props.currentCount}</Text>
      </View>
      <TouchableOpacity
        style={counterActionable}
        onPress={() => props.modifyCounter(props.formField, '+')}
      >
        <Text style={counterControlsText}>+</Text>
      </TouchableOpacity>
    </View>
  </View>
);

CounterButton.propTypes = {
  formField: string,
  label: string,
  modifyCounter: func,
  currentCount: number,
  labelComplement: string,
  labelIcon: node
};

CounterButton.defaultProps = {
  formField: '',
  label: '',
  modifyCounter: noop,
  currentCount: 0,
  labelComplement: '',
  labelIcon: null
};

export default CounterButton;
