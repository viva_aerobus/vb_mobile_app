import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { string, func, shape } from 'prop-types';

import styles from './styles';

const {
  formButtonsContainer,
  cancelButtonWrapper,
  cancelButton,
  whiteText,
  typographyMedium,
  submitButtonWrapper,
  submitButton
} = styles;

const FormButtons = props => (
  <View style={formButtonsContainer}>
    <View style={cancelButtonWrapper}>
      <TouchableOpacity onPress={props.onCancel}>
        <View style={cancelButton}>
          <Text style={[whiteText, typographyMedium]} fontCase="bold">
            {props.formButtonsLabels.cancel}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
    <View style={submitButtonWrapper}>
      <TouchableOpacity onPress={props.onSubmit}>
        <View style={submitButton}>
          <Text style={[whiteText, typographyMedium]} fontCase="bold">
            {props.formButtonsLabels.submit}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  </View>
);

FormButtons.propTypes = {
  onSubmit: func.isRequired,
  onCancel: func.isRequired,
  formButtonsLabels: shape({
    cancel: string,
    submit: string
  }).isRequired
};

export default FormButtons;
