import React from 'react';
import { View, Text } from 'react-native';
import { node, string } from 'prop-types';

import styles from './styles';

const {
  pOFormFieldContainer,
  formFieldLabelWrapper,
  formFieldInputWrapper,
  formLabelTextSize,
  darkGrayText,
  alignRight
} = styles;

const FormField = props => (
  <View style={pOFormFieldContainer}>
    <View style={formFieldLabelWrapper}>
      <Text style={[formLabelTextSize, darkGrayText, alignRight]}>
        {props.label}
      </Text>
    </View>
    <View style={formFieldInputWrapper}>{props.children}</View>
  </View>
);

FormField.propTypes = {
  children: node.isRequired,
  label: string
};

FormField.defaultProps = {
  label: ''
};

export default FormField;
