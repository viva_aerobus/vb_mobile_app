import React from 'react';
import { View, Text, Image } from 'react-native';
import { string } from 'prop-types';

import styles from './styles';

const {
  headerFrame,
  headerContainer,
  headerImage,
  headerLabelWrapper,
  formHeaderText
} = styles;

const plane = require('../../../assets/vbImages/plane.png');

const FormField = props => (
  <View style={headerFrame}>
    <View style={headerContainer}>
      <Image style={headerImage} source={plane} />
      <View style={headerLabelWrapper}>
        <Text style={formHeaderText}>{props.headerLabel}</Text>
      </View>
    </View>
  </View>
);

FormField.propTypes = {
  headerLabel: string
};

FormField.defaultProps = {
  headerLabel: ''
};

export default FormField;
