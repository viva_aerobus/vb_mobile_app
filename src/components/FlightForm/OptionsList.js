import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import noop from 'lodash/noop';
import { string, number, arrayOf, func } from 'prop-types';

import styles from './styles';

const {
  optionsContainerVertical,
  optionsContainerHorizontal,
  optionContainer,
  optionLabelWrapper,
  counterActionable,
  selectedMark,
  formLabelTextSize,
  darkGrayText,
  alignRight
} = styles;

const OptionsList = props => {
  const generateOptions = options =>
    options.map((option, index) => (
      <View style={optionContainer}>
        <View style={optionLabelWrapper}>
          <Text style={[formLabelTextSize, darkGrayText, alignRight]}>
            {option}
          </Text>
        </View>
        <TouchableOpacity
          style={counterActionable}
          onPress={() => props.onOptionSelection(index)}
        >
          {index === props.selectedIndex ? <View style={selectedMark} /> : null}
        </TouchableOpacity>
      </View>
    ));
  const optionsContainerStyle =
    props.distribution === 'Vertical'
      ? optionsContainerVertical
      : optionsContainerHorizontal;
  return (
    <View style={optionsContainerStyle}>{generateOptions(props.options)}</View>
  );
};

OptionsList.propTypes = {
  options: arrayOf(string),
  selectedIndex: number,
  distribution: string,
  onOptionSelection: func
};

OptionsList.defaultProps = {
  options: [],
  selectedIndex: 0,
  distribution: 'Vertical',
  onOptionSelection: noop
};

export default OptionsList;
