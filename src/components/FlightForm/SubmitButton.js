import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import noop from 'lodash/noop';
import { string, func, node } from 'prop-types';

import styles from './styles';

const {
  largeSubmitButton,
  submitButtonText,
  submitButtonFrame,
  submitButtonTextWrapper,
  submitButtonImage
} = styles;

const SubmitButton = props => (
  <View style={submitButtonFrame}>
    <TouchableOpacity onPress={props.onPress} style={largeSubmitButton}>
      <Image style={submitButtonImage} source={props.icon} />
      <View style={submitButtonTextWrapper}>
        <Text style={submitButtonText}>{props.label}</Text>
      </View>
    </TouchableOpacity>
  </View>
);

SubmitButton.propTypes = {
  label: string,
  icon: node,
  onPress: func
};

SubmitButton.defaultProps = {
  label: '',
  icon: null,
  onPress: noop
};

export default SubmitButton;
