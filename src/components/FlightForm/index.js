import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, Keyboard } from 'react-native';
import moment from 'moment';
import { string, shape, func, bool, arrayOf, number } from 'prop-types';
import noop from 'lodash/noop';
import FormHeader from './FormHeader';
import SubmitButton from './SubmitButton';
import FormSelectionButton from '../FormSelectionButton';
import Tabs from '../Tabs';
import styles from './styles';

const departureIcon = require('../../../assets/vbImages/departure.png');
const arrivalIcon = require('../../../assets/vbImages/arrival.png');
const calendarIcon = require('../../../assets/vbImages/calendar.png');
const passengerIcon = require('../../../assets/vbImages/passenger.png');
const searchIcon = require('../../../assets/vbImages/search.png');
const smallPlane = require('../../../assets/vbImages/smallPlane.png');

class FlightForm extends Component {
  componentDidMount() {
    console.log('FlightForm loaded');
  }
  handleDestinationPressed = () => {
    this.props.setModalPickerOptions(
      this.props.airportsArray,
      'Elige el destino: ',
      'Aeropuertos',
      'destiny',
      smallPlane
    );
    this.props.updateModalStatus();
  };
  handleDeparturePressed = () => {
    this.props.setModalPickerOptions(
      this.props.airportsArray,
      'Elige el origen: ',
      'Aeropuertos',
      'origin',
      smallPlane
    );
    this.props.updateModalStatus();
  };
  handleDepartureDatePressed = () => {
    this.props.setModalCalendarOptions(
      'Selecciona una fecha de ida',
      'departureDate'
    );
    this.props.updateModalCalendarStatus();
  };
  handleReturnDatePressed = () => {
    this.props.setModalCalendarOptions(
      'Selecciona una fecha de regreso',
      'returnDate'
    );
    this.props.updateModalCalendarStatus();
  };
  render() {
    const {
      pODetailsEntriesContainer,
      formFrame,
      formSelectionButtonRow
    } = styles;
    const {
      placeholders,
      isOneWayTicket,
      onSubmit,
      values,
      tripOptions,
      onOptionSelection,
      updateModalPassengersStatus
    } = this.props;
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={formFrame}>
          <FormHeader headerLabel="Información de tu viaje" />
          <Tabs
            labelsList={tripOptions}
            onTabSelection={onOptionSelection}
            selectedIndex={values.selectedTripOptionIndex}
          />
          <View style={pODetailsEntriesContainer}>
            <View style={formSelectionButtonRow}>
              <FormSelectionButton
                label="Desde"
                icon={departureIcon}
                iconFirst
                onPress={this.handleDeparturePressed}
                value={values.origin[0]}
              />
              <FormSelectionButton
                label="Hacia"
                icon={arrivalIcon}
                onPress={this.handleDestinationPressed}
                iconFirst
                value={values.destiny[0]}
              />
            </View>
            <View style={formSelectionButtonRow}>
              <FormSelectionButton
                label="Fecha de salida"
                icon={calendarIcon}
                iconFirst
                onPress={this.handleDepartureDatePressed}
                value={
                  values.departureDate
                    ? moment(values.departureDate, 'YYYY-MM-DD').format(
                        'DD.MM.YYYY'
                      )
                    : ''
                }
              />
              <FormSelectionButton
                label="Pasajeros"
                icon={passengerIcon}
                iconFirst
                onPress={updateModalPassengersStatus}
              />
            </View>
            {!isOneWayTicket ? (
              <View style={formSelectionButtonRow}>
                <FormSelectionButton
                  label="Fecha de regreso"
                  icon={calendarIcon}
                  iconFirst
                  onPress={this.handleReturnDatePressed}
                  value={
                    values.returnDate
                      ? moment(values.returnDate, 'YYYY-MM-DD').format(
                          'DD.MM.YYYY'
                        )
                      : ''
                  }
                />
              </View>
            ) : null}
            <SubmitButton
              label="Buscar Vuelos"
              icon={searchIcon}
              onPress={onSubmit}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

/*
<Text style={[lightBlueText, formLabelTextSize]}>
  {values.returnDate
    ? moment(values.returnDate, 'YYYY-MM-DD').format(
        'DD.MM.YYYY'
      )
    : placeholders.returnDate}
</Text>
*/

FlightForm.propTypes = {
  placeholders: shape({
    origin: string,
    destiny: string,
    currency: string
  }),
  values: shape({
    origin: string,
    destiny: string,
    currency: string
  }),
  isOneWayTicket: bool,
  formButtonsLabels: shape({
    cancel: string,
    submit: string
  }),
  onSubmit: func,
  onCancel: func,
  setModalPickerOptions: func,
  currenciesArray: arrayOf(
    shape({
      label: string,
      value: string,
      index: number
    })
  ),
  airportsArray: arrayOf(
    shape({
      label: string,
      value: string,
      index: number
    })
  ),
  updateModalStatus: func,
  modifyCounter: func,
  tripOptions: arrayOf(string),
  onOptionSelection: func,
  updateModalCalendarStatus: func,
  setModalCalendarOptions: func,
  updateModalPassengersStatus: func
};

FlightForm.defaultProps = {
  placeholders: {
    origin: '',
    destiny: ''
  },
  values: {
    origin: '',
    destiny: ''
  },
  isOneWayTicket: bool,
  formButtonsLabels: shape({
    cancel: '',
    submit: ''
  }),
  onSubmit: noop,
  onCancel: noop,
  setModalPickerOptions: noop,
  airportsArray: [],
  currenciesArray: [],
  updateModalStatus: noop,
  modifyCounter: noop,
  tripOptions: [],
  onOptionSelection: noop,
  updateModalCalendarStatus: noop,
  setModalCalendarOptions: noop,
  updateModalPassengersStatus: noop
};

export default FlightForm;
