import { StyleSheet, Dimensions } from 'react-native';

import { scalingFactor } from '../../vb_data_layer/styles/constants';

import {
  darkGray,
  mediumGray,
  lightBlue,
  darkBlue,
  vbLightBackground,
  vbGreenActive,
  vbGreenText,
  vbDarkGreenText
} from '../../vb_data_layer/styles/colors';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  formLabelTextSize: {
    fontSize: (screenWidth / 23) * scalingFactor
  },
  typographyMedium: {
    fontSize: (screenWidth / 19) * scalingFactor
  },
  labelComplementTextSize: {
    fontSize: (screenWidth / 28) * scalingFactor
  },
  darkGrayText: {
    color: darkGray
  },
  whiteText: {
    color: '#FFF'
  },
  lightBlueText: {
    color: lightBlue
  },
  darkBlueText: {
    color: darkBlue
  },
  pODetailsEntriesContainer: {
    width: screenWidth * 0.92,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: screenHeight * 0.03,
    backgroundColor: vbLightBackground,
    height: screenHeight * 0.66,
    marginTop: screenHeight * 0.02,
    borderRadius: 10
  },
  formFrame: {
    width: screenWidth,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: screenHeight * 0.03
  },
  formFieldInputWrapper: {
    width: screenWidth * 0.6,
    height: screenHeight * 0.15,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.02
  },
  formButtonsContainer: {
    width: screenWidth,
    height: screenHeight * 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  cancelButtonWrapper: {
    width: screenWidth * 0.35,
    height: screenHeight * 0.1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: screenWidth * 0.02
  },
  cancelButton: {
    backgroundColor: vbGreenActive,
    width: screenWidth * 0.3,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25
  },
  submitButtonWrapper: {
    width: screenWidth * 0.65,
    height: screenHeight * 0.1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.02
  },
  submitButton: {
    backgroundColor: vbGreenActive,
    width: screenWidth * 0.6,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25
  },
  purchaseOrderFormInput: {
    width: screenWidth * 0.5,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: mediumGray,
    backgroundColor: '#FFF',
    paddingLeft: screenWidth * 0.02
  },
  inputStyle: {
    color: lightBlue,
    width: screenWidth * 0.43,
    height: screenHeight * 0.06,
    textAlign: 'left',
    paddingLeft: screenWidth * 0.02,
    fontSize: (screenWidth / 25) * scalingFactor
  },
  notEditableInputStyle: {
    color: darkGray,
    width: screenWidth * 0.43,
    height: screenHeight * 0.06,
    textAlign: 'left',
    paddingLeft: screenWidth * 0.02,
    fontSize: (screenWidth / 25) * scalingFactor
  },
  inputContainerStyle: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: mediumGray,
    backgroundColor: '#FFF',
    width: screenWidth * 0.5,
    height: screenHeight * 0.06,
    flexDirection: 'row'
  },
  deleteButton: {
    width: screenWidth * 0.07,
    height: screenHeight * 0.06,
    alignItems: 'center',
    justifyContent: 'center'
  },
  alignRight: {
    textAlign: 'right'
  },
  formSectionHeader: {
    width: screenWidth,
    height: screenHeight * 0.04,
    justifyContent: 'center',
    alignItems: 'center'
  },
  optionsContainerVertical: {
    width: screenWidth,
    alignItems: 'center',
    paddingVertical: screenHeight * 0.01
  },
  optionsContainerHorizontal: {
    width: screenWidth,
    height: screenHeight * 0.08,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: screenWidth * 0.05
  },
  optionContainer: {
    width: screenWidth * 0.42,
    height: screenHeight * 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: screenWidth * 0.02,
    flexDirection: 'row'
  },
  optionLabelWrapper: {
    width: screenWidth * 0.38,
    height: screenHeight * 0.15,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: screenWidth * 0.02
  },
  selectedMark: {
    borderRadius: 7,
    borderWidth: 1,
    borderColor: mediumGray,
    backgroundColor: darkBlue,
    width: screenHeight * 0.04,
    height: screenHeight * 0.04
  },
  headerFrame: {
    width: screenWidth,
    height: screenHeight * 0.15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerContainer: {
    width: screenWidth * 0.85,
    height: screenHeight * 0.15,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  headerImage: {
    height: screenHeight * 0.1,
    width: screenWidth * 0.2,
    resizeMode: 'contain'
  },
  headerLabelWrapper: {
    height: screenHeight * 0.15,
    width: screenWidth * 0.55,
    paddingLeft: screenWidth * 0.04,
    paddingTop: screenHeight * 0.02
  },
  formHeaderText: {
    fontWeight: '700',
    fontSize: (screenWidth / 12) * scalingFactor,
    flexWrap: 'wrap',
    flex: 1,
    color: '#FFF'
  },
  formSelectionButtonRow: {
    height: screenHeight * 0.14,
    width: screenWidth * 0.92,
    position: 'relative',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row'
  },
  submitButtonFrame: {
    height: screenHeight * 0.11,
    width: screenWidth,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: screenHeight * 0.03
  },
  largeSubmitButton: {
    height: screenHeight * 0.11,
    width: screenWidth * 0.85,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: vbGreenActive,
    borderRadius: 6
  },
  submitButtonText: {
    fontWeight: '500',
    fontSize: (screenWidth / 18) * scalingFactor,
    color: '#FFF'
  },
  submitButtonTextWrapper: {
    width: screenWidth * 0.4,
    height: screenHeight * 0.11,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.02
  },
  submitButtonImage: {
    height: screenHeight * 0.11,
    width: screenWidth * 0.085,
    resizeMode: 'contain'
  },
  pOFormFieldContainer: {
    width: screenWidth,
    height: screenHeight * 0.12,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  formFieldLabelWrapper: {
    width: screenWidth * 0.4,
    height: screenHeight * 0.12,
    justifyContent: 'center',
    alignItems: 'center'
  },
  labelTitleWrapper: {
    width: screenWidth * 0.4,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingLeft: screenWidth * 0.08
  },
  backButton: {
    height: screenHeight * 0.07,
    width: screenWidth * 0.07,
    resizeMode: 'contain'
  },
  lableTextWrapper: {
    width: screenWidth * 0.3,
    height: screenHeight * 0.07,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.02
  },
  labelComplementWrapper: {
    width: screenWidth * 0.4,
    height: screenHeight * 0.03,
    justifyContent: 'center',
    alignItems: 'center'
  },
  counterButtonLabelText: {
    fontWeight: '500',
    fontSize: (screenWidth / 19) * scalingFactor,
    color: vbGreenText
  },
  counterActionable: {
    borderRadius: screenHeight * 0.06,
    borderWidth: 2,
    borderColor: vbDarkGreenText,
    backgroundColor: '#FFF',
    width: screenHeight * 0.06,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'center'
  },
  currentCountWrapper: {
    borderRadius: 4,
    backgroundColor: '#FFF',
    width: screenHeight * 0.09,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'center'
  },
  counterControlsText: {
    fontWeight: '600',
    fontSize: (screenWidth / 15) * scalingFactor,
    color: vbDarkGreenText
  },
  countButtonInputWrapper: {
    width: screenWidth * 0.6,
    height: screenHeight * 0.15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: screenWidth * 0.02,
    flexDirection: 'row'
  }
});

export default styles;
