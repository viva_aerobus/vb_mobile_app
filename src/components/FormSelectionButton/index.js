import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import noop from 'lodash/noop';
import { string, func, bool, node } from 'prop-types';

import styles from './styles';

const {
  selectionButtonContainer,
  buttonLabelWrapper,
  buttonText,
  selectionButtonImage,
  buttonTextWrapper,
  buttonValueWrapper,
  formSelectionValueText
} = styles;

const FormSelectionButton = props => {
  const iconImage = <Image style={selectionButtonImage} source={props.icon} />;
  return (
    <TouchableOpacity style={selectionButtonContainer} onPress={props.onPress}>
      <View style={buttonLabelWrapper}>
        {props.iconFirst ? iconImage : null}
        <View style={buttonTextWrapper}>
          <Text style={buttonText}>{props.label}</Text>
        </View>
        {!props.iconFirst ? iconImage : null}
      </View>
      <View style={buttonValueWrapper}>
        <Text style={formSelectionValueText}>{props.value}</Text>
      </View>
    </TouchableOpacity>
  );
};

FormSelectionButton.propTypes = {
  label: string,
  icon: node,
  iconFirst: bool,
  onPress: func,
  value: string
};

FormSelectionButton.defaultProps = {
  label: '',
  icon: null,
  iconFirst: false,
  onPress: noop,
  value: string
};

export default FormSelectionButton;
