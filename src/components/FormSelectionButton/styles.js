import { StyleSheet, Dimensions } from 'react-native';

import { scalingFactor } from '../../vb_data_layer/styles/constants';

import {
  darkGray,
  lightBlue,
  darkBlue,
  vbDarkGreenText,
  vbMediumGray
} from '../../vb_data_layer/styles/colors';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  formLabelTextSize: {
    fontSize: (screenWidth / 23) * scalingFactor
  },
  typographyMedium: {
    fontSize: (screenWidth / 19) * scalingFactor
  },
  darkGrayText: {
    color: darkGray
  },
  whiteText: {
    color: '#FFF'
  },
  lightBlueText: {
    color: lightBlue
  },
  darkBlueText: {
    color: darkBlue
  },
  selectionButtonContainer: {
    width: screenWidth * 0.4,
    height: screenHeight * 0.12,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(249,249,249,1)',
    borderRadius: 8
  },
  buttonLabelWrapper: {
    width: screenWidth * 0.3,
    height: screenHeight * 0.07,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  buttonText: {
    fontWeight: '700',
    fontSize: (screenWidth / 23) * scalingFactor,
    color: vbDarkGreenText
  },
  buttonTextWrapper: {
    width: screenWidth * 0.23,
    height: screenHeight * 0.07,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  selectionButtonImage: {
    height: screenHeight * 0.07,
    width: screenWidth * 0.08,
    resizeMode: 'contain'
  },
  buttonValueWrapper: {
    width: screenWidth * 0.38,
    height: screenHeight * 0.05,
    alignItems: 'center',
    justifyContent: 'center'
  },
  formSelectionValueText: {
    fontWeight: '500',
    fontSize: (screenWidth / 24) * scalingFactor,
    color: vbMediumGray
  }
});

export default styles;
