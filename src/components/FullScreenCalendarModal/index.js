import React from 'react';
import { Modal, Image, View, Text, TouchableOpacity } from 'react-native';
import { string, func, bool } from 'prop-types';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import noop from 'lodash/noop';
import styles from './styles';

const {
  modalContainer,
  confirmationModalWrapper,
  modalHeader,
  modalBackButtonWrapper,
  backButton,
  headerLabelWrapper,
  modalHeaderText,
  fullScreenModalHeadersContainer,
  calendarContainer
} = styles;

const backIcon = require('../../../assets/vbImages/chevronLeft.png');

const FullScreenCalendarModal = props => {
  const {
    modalVisible,
    hideModal,
    headerLabel,
    onDateSelect,
    selectedDate
  } = props;
  LocaleConfig.locales.es = {
    monthNames: [
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Agosto',
      'Septiembre',
      'Octubre',
      'Noviembre',
      'Diciembre'
    ],
    monthNamesShort: [
      'Ene.',
      'Feb.',
      'Marzo',
      'Abril',
      'Mayo',
      'Jun.',
      'Jul.',
      'Ago.',
      'Sept.',
      'Oct.',
      'Nov.',
      'Dic.'
    ],
    dayNames: [
      'Domingo',
      'Lunes',
      'Martes',
      'Miercoles',
      'Jueves',
      'Viernes',
      'Sábado'
    ],
    dayNamesShort: ['Do', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sa'],
    today: 'Hoy'
  };
  LocaleConfig.defaultLocale = 'es';
  return (
    <Modal
      transparent
      animationType="none"
      visible={modalVisible}
      supportedOrientations={['portrait']}
    >
      <View style={modalContainer}>
        <View style={confirmationModalWrapper}>
          <View style={fullScreenModalHeadersContainer}>
            <View style={modalHeader}>
              <TouchableOpacity
                style={modalBackButtonWrapper}
                onPress={hideModal}
              >
                <Image style={backButton} source={backIcon} />
              </TouchableOpacity>
              <View style={headerLabelWrapper}>
                <Text style={modalHeaderText}>{headerLabel}</Text>
              </View>
            </View>
          </View>
          <View style={calendarContainer}>
            <Calendar
              minDate={Date()}
              // Handler which gets executed on day press. Default = undefined
              onDayPress={day => {
                onDateSelect(day.dateString);
                hideModal();
              }}
              // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
              monthFormat="yyyy MMMM"
              theme={{
                selectedDayBackgroundColor: '#00adf5',
                selectedDayTextColor: '#ffffff'
              }}
              markedDates={{
                [selectedDate]: { selected: true }
              }}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

FullScreenCalendarModal.propTypes = {
  headerLabel: string,
  hideModal: func,
  modalVisible: bool,
  onDateSelect: func,
  selectedDate: string
};

FullScreenCalendarModal.defaultProps = {
  headerLabel: '',
  hideModal: noop,
  modalVisible: false,
  onDateSelect: noop,
  selectedDate: ''
};

export default FullScreenCalendarModal;
