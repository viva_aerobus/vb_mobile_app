import { StyleSheet, Dimensions } from 'react-native';
import {
  darkBlue,
  lightBlue,
  darkGray,
  divisionBarGray,
  vbGreenModalHeader,
  vbLightGray,
  vbMediumGray,
  vbDarkGreenText,
  vbItemTextGray
} from '../../vb_data_layer/styles/colors';
import { scalingFactor } from '../../vb_data_layer/styles/constants';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default StyleSheet.create({
  typographySmall: {
    fontSize: (screenWidth / 28) * scalingFactor
  },
  typographyMedium: {
    fontSize: (screenWidth / 19) * scalingFactor
  },
  blueNavBarText: {
    color: darkBlue
  },
  lightBlueText: {
    color: lightBlue
  },
  darkGrayText: {
    color: darkGray
  },
  darkGreenText: {
    color: vbDarkGreenText
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  confirmationModalShade: {
    width: screenWidth,
    height: screenHeight * 0.65,
    backgroundColor: '#00000077'
  },
  confirmationModalWrapper: {
    width: screenWidth,
    height: screenHeight,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: screenHeight * 0.03
  },
  modalTextWrapper: {
    width: screenWidth,
    height: screenHeight * 0.09,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    flexDirection: 'row',
    backgroundColor: vbLightGray
  },
  modalTextBorder: {
    width: screenWidth * 0.5,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.01
  },
  modalOptionsWrapper: {
    width: screenWidth,
    height: screenHeight * 0.14
  },
  pVModalOptionBorder: {
    height: screenHeight * 0.05,
    width: screenWidth * 0.6,
    borderBottomWidth: 1,
    borderBottomColor: divisionBarGray,
    position: 'absolute',
    zIndex: 0
  },
  modalHeader: {
    height: screenHeight * 0.16,
    width: screenWidth,
    backgroundColor: vbGreenModalHeader,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  modalBackButtonWrapper: {
    height: screenHeight * 0.12,
    width: screenWidth * 0.1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backButton: {
    height: screenHeight * 0.12,
    width: screenWidth * 0.1,
    resizeMode: 'contain'
  },
  headerLabelWrapper: {
    height: screenHeight * 0.12,
    width: screenWidth * 0.8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalHeaderText: {
    fontWeight: '700',
    fontSize: (screenWidth / 16) * scalingFactor,
    color: '#FFF'
  },
  modalCommentText: {
    fontWeight: '500',
    fontSize: (screenWidth / 18) * scalingFactor,
    color: vbMediumGray
  },
  commentIcon: {
    height: screenHeight * 0.04,
    width: screenWidth * 0.1,
    resizeMode: 'contain'
  },
  commentIconWrapper: {
    height: screenHeight * 0.048,
    width: screenWidth * 0.15,
    alignItems: 'flex-end'
  },
  modalListITemContainer: {
    height: screenHeight * 0.1,
    width: screenWidth,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
    borderBottomWidth: 2,
    borderBottomColor: vbLightGray
  },
  itemNameContainer: {
    height: screenHeight * 0.1,
    width: screenWidth * 0.7,
    justifyContent: 'center',
    alignItems: 'center'
  },
  airportNameWrapper: {
    height: screenHeight * 0.06,
    width: screenWidth * 0.7,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.03
  },
  countryCodeWrapper: {
    height: screenHeight * 0.04,
    width: screenWidth * 0.7,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.03
  },
  countryCodeText: {
    fontWeight: '500',
    fontSize: (screenWidth / 18) * scalingFactor,
    color: vbItemTextGray
  },
  airportCodeWrapper: {
    height: screenHeight * 0.09,
    width: screenWidth * 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: screenWidth * 0.05
  },
  airportCodeText: {
    fontWeight: '500',
    fontSize: (screenWidth / 15) * scalingFactor,
    color: vbItemTextGray
  },
  fullScreenModalHeadersContainer: {
    height: screenHeight * 0.28,
    width: screenWidth,
    position: 'relative'
  },
  searchBarFrame: {
    position: 'absolute',
    top: screenHeight * 0.13,
    height: screenHeight * 0.06,
    width: screenWidth,
    zIndex: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchBarContainer: {
    flexDirection: 'row',
    width: screenWidth * 0.8,
    height: screenHeight * 0.045,
    backgroundColor: '#FFF',
    borderRadius: 3,
    borderColor: vbLightGray,
    borderWidth: 2,
    paddingLeft: screenWidth * 0.05
  },
  searchBarInput: {
    width: screenWidth * 0.65,
    height: screenHeight * 0.045,
    fontSize: (screenWidth / 19) * scalingFactor,
    color: vbDarkGreenText
  },
  searchBarIconWrapper: {
    width: screenWidth * 0.1,
    height: screenHeight * 0.045,
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchBlack: {
    width: screenWidth * 0.07,
    height: screenHeight * 0.045,
    resizeMode: 'contain'
  },
  calendarContainer: {
    width: screenWidth,
    height: 'auto'
  }
});
