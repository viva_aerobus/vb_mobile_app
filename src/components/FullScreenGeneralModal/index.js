import React from 'react';
import { Modal, Image, View, Text, TouchableOpacity } from 'react-native';
import { string, func, bool, oneOfType, arrayOf, node } from 'prop-types';
import noop from 'lodash/noop';
import styles from './styles';

const {
  modalContainer,
  confirmationModalWrapper,
  modalHeader,
  modalBackButtonWrapper,
  backButton,
  headerLabelWrapper,
  modalHeaderText,
  fullScreenModalHeadersContainer,
  calendarContainer
} = styles;

const backIcon = require('../../../assets/vbImages/chevronLeft.png');

const FullScreenCalendarModal = props => {
  const { modalVisible, hideModal, headerLabel, children } = props;
  return (
    <Modal
      transparent
      animationType="none"
      visible={modalVisible}
      supportedOrientations={['portrait']}
    >
      <View style={modalContainer}>
        <View style={confirmationModalWrapper}>
          <View style={fullScreenModalHeadersContainer}>
            <View style={modalHeader}>
              <TouchableOpacity
                style={modalBackButtonWrapper}
                onPress={hideModal}
              >
                <Image style={backButton} source={backIcon} />
              </TouchableOpacity>
              <View style={headerLabelWrapper}>
                <Text style={modalHeaderText}>{headerLabel}</Text>
              </View>
            </View>
          </View>
          <View style={calendarContainer}>{children}</View>
        </View>
      </View>
    </Modal>
  );
};

FullScreenCalendarModal.propTypes = {
  headerLabel: string,
  hideModal: func,
  modalVisible: bool,
  children: oneOfType([node, arrayOf(node)])
};

FullScreenCalendarModal.defaultProps = {
  headerLabel: '',
  hideModal: noop,
  modalVisible: false,
  children: null
};

export default FullScreenCalendarModal;
