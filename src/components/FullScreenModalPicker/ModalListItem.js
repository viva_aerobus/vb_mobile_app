import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import noop from 'lodash/noop';
import { string, func, bool, number } from 'prop-types';

import styles from './styles';

const {
  modalListITemContainer,
  typographyMedium,
  lightBlueText,
  modalListItemFrame,
  itemNameContainer,
  airportNameWrapper,
  countryCodeWrapper,
  countryCodeText,
  airportCodeWrapper,
  airportCodeText,
  darkGreenText
} = styles;

const ModalListItem = props => {
  const textStyles = props.isSelected
    ? [typographyMedium, lightBlueText]
    : [typographyMedium, darkGreenText];
  return (
    <TouchableOpacity
      onPress={() => {
        props.onOptionPress(props.stateField, [
          props.label,
          props.value,
          props.index
        ]);
        props.cleanQueryString();
      }}
      style={modalListItemFrame}
    >
      <View style={modalListITemContainer}>
        <View style={itemNameContainer}>
          <View style={airportNameWrapper}>
            <Text style={textStyles}>{props.label}</Text>
          </View>
          <View style={countryCodeWrapper}>
            <Text style={countryCodeText}>{props.country}</Text>
          </View>
        </View>
        <View style={airportCodeWrapper}>
          <Text style={airportCodeText}>{props.value}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

ModalListItem.propTypes = {
  onOptionPress: func,
  label: string,
  value: string,
  index: number,
  country: string,
  isSelected: bool,
  stateField: string,
  cleanQueryString: func
};

ModalListItem.defaultProps = {
  label: '',
  onOptionPress: noop,
  value: '',
  index: -1,
  isSelected: false,
  stateField: '',
  country: '',
  cleanQueryString: noop
};

export default ModalListItem;
