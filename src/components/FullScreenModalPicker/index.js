import React, { Component } from 'react';
import {
  Modal,
  Image,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';
import { string, func, bool, number, arrayOf, shape, node } from 'prop-types';
import noop from 'lodash/noop';
import ModalListItem from './ModalListItem';
import styles from './styles';

const {
  modalContainer,
  confirmationModalWrapper,
  modalTextWrapper,
  modalOptionsWrapper,
  modalTextBorder,
  modalHeader,
  modalBackButtonWrapper,
  backButton,
  headerLabelWrapper,
  modalHeaderText,
  modalCommentText,
  commentIcon,
  commentIconWrapper,
  fullScreenModalHeadersContainer,
  searchBarFrame,
  searchBarContainer,
  searchBarInput,
  searchBarIconWrapper,
  searchBlack
} = styles;

const backIcon = require('../../../assets/vbImages/chevronLeft.png');
const searchBlackIcon = require('../../../assets/vbImages/searchBlack.png');

class FullScreenModalPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      queryString: ''
    };
  }
  generateOptionsMarkup = () => {
    const optionsToShow =
      this.state.queryString !== ''
        ? this.flightSearch(this.props.options)
        : this.props.options;
    return optionsToShow.map((item, index) => (
      <ModalListItem
        isSelected={this.props.selectedIndex === index}
        onOptionPress={this.props.onOptionPress}
        label={item.label}
        value={item.value}
        index={item.index}
        country={item.country}
        stateField={this.props.stateField}
        key={item.label}
        cleanQueryString={() => {
          this.setState({ queryString: '' });
        }}
      />
    ));
  };
  handleQueryStringChanged = value => {
    this.setState({ queryString: value });
  };
  flightSearch = options => {
    const { queryString } = this.state;
    return options.filter(
      item =>
        item.label.toLowerCase().includes(queryString.toLowerCase()) ||
        item.value.toLowerCase().includes(queryString.toLowerCase())
    );
  };
  render() {
    const {
      modalVisible,
      hideModal,
      modalCommentIcon,
      headerLabel,
      comment,
      withSearchBar
    } = this.props;
    return (
      <Modal
        transparent
        animationType="none"
        visible={modalVisible}
        supportedOrientations={['portrait']}
      >
        <View style={modalContainer}>
          <View style={confirmationModalWrapper}>
            <View style={fullScreenModalHeadersContainer}>
              <View style={modalHeader}>
                <TouchableOpacity
                  style={modalBackButtonWrapper}
                  onPress={() => {
                    this.setState({ queryString: '' });
                    hideModal();
                  }}
                >
                  <Image style={backButton} source={backIcon} />
                </TouchableOpacity>
                <View style={headerLabelWrapper}>
                  <Text style={modalHeaderText}>{headerLabel}</Text>
                </View>
              </View>
              {withSearchBar ? (
                <View style={searchBarFrame}>
                  <View style={searchBarContainer}>
                    <TextInput
                      style={searchBarInput}
                      placeholder="Buscar ciudad o aeropuerto"
                      onChangeText={value =>
                        this.handleQueryStringChanged(value)
                      }
                    />
                    <View style={searchBarIconWrapper}>
                      <Image style={searchBlack} source={searchBlackIcon} />
                    </View>
                  </View>
                </View>
              ) : null}
              <View style={modalTextWrapper}>
                {modalCommentIcon ? (
                  <View style={commentIconWrapper}>
                    <Image style={commentIcon} source={modalCommentIcon} />
                  </View>
                ) : null}
                <View style={modalTextBorder}>
                  <Text style={modalCommentText}>{comment}</Text>
                </View>
              </View>
            </View>
            <ScrollView style={modalOptionsWrapper}>
              {this.generateOptionsMarkup()}
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

FullScreenModalPicker.propTypes = {
  options: arrayOf(
    shape({
      label: string,
      value: string,
      index: number,
      country: string
    })
  ),
  headerLabel: string,
  hideModal: func,
  modalVisible: bool,
  selectedIndex: number,
  onOptionPress: func.isRequired,
  stateField: string,
  comment: string,
  modalCommentIcon: node,
  withSearchBar: bool
};

FullScreenModalPicker.defaultProps = {
  options: [],
  headerLabel: '',
  hideModal: noop,
  modalVisible: false,
  selectedIndex: 0,
  stateField: '',
  comment: '',
  modalCommentIcon: null,
  withSearchBar: false
};

export default FullScreenModalPicker;
