import React from 'react';
import { SafeAreaView } from 'react-navigation';
import { oneOfType, node, arrayOf, string, bool } from 'prop-types';
import GradientBackground from '../LayoutBackground';
import ImageBackground from '../LayoutBackground/ImageBackground';
import styles from './styles';

const { layoutContainer } = styles;

export default function LayoutContainer(props) {
  const { children, backgroundColor, customBackground } = props;
  if (customBackground) {
    return (
      <ImageBackground>
        <SafeAreaView style={[layoutContainer, { backgroundColor }]}>
          {children}
        </SafeAreaView>
      </ImageBackground>
    );
  }
  return (
    <GradientBackground>
      <SafeAreaView style={[layoutContainer, { backgroundColor }]}>
        {children}
      </SafeAreaView>
    </GradientBackground>
  );
}

LayoutContainer.propTypes = {
  children: oneOfType([arrayOf(node), node]),
  backgroundColor: string,
  customBackground: bool
};

LayoutContainer.defaultProps = {
  children: [],
  backgroundColor: 'none',
  customBackground: false
};
