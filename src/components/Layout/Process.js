import React from 'react';
import { ActivityIndicator } from 'react-native';
import Container from './Container';

export default function Layout() {
  return (
    <Container>
      <ActivityIndicator size="large" color="#969696" style={{ flex: 1 }} />
    </Container>
  );
}
