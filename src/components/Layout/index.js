import React from 'react';
import { oneOfType, node, arrayOf, string, bool } from 'prop-types';
import Process from './Process';
import Container from './Container';

const Layout = props => {
  const { children, process, backgroundColor, customBackground } = props;
  if (process) {
    return <Process />;
  }
  return (
    <Container
      backgroundColor={backgroundColor}
      customBackground={customBackground}
    >
      {children}
    </Container>
  );
};

Layout.propTypes = {
  children: oneOfType([arrayOf(node), node]),
  process: string,
  backgroundColor: string,
  customBackground: bool
};

Layout.defaultProps = {
  children: [],
  process: null,
  backgroundColor: 'none',
  customBackground: false
};

export default Layout;
