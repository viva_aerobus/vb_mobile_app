import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  layoutContainer: {
    flex: 1,
    width: '100%'
  },
  activityIndicator: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default styles;
