import React from 'react';
import { ImageBackground } from 'react-native';
import { oneOfType, node, arrayOf } from 'prop-types';
import styles from './styles';

const { fullScreen } = styles;

const layoutImageBackground = require('../../../assets/vbBackgroundDark.png');

export default function BackgroundImage({ children }) {
  return (
    <ImageBackground source={layoutImageBackground} style={fullScreen}>
      {children}
    </ImageBackground>
  );
}

BackgroundImage.propTypes = {
  children: oneOfType([arrayOf(node), node])
};

BackgroundImage.defaultProps = {
  children: []
};
