import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { oneOfType, node, arrayOf } from 'prop-types';
import { listingsGradientSteps } from '../../vb_data_layer/styles/colors';
import styles from './styles';

const { gradient } = styles;

export default function BackgroundGradient({ children }) {
  return (
    <LinearGradient colors={listingsGradientSteps} style={gradient}>
      {children}
    </LinearGradient>
  );
}

BackgroundGradient.propTypes = {
  children: oneOfType([arrayOf(node), node])
};

BackgroundGradient.defaultProps = {
  children: []
};
