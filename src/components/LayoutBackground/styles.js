import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  gradient: {
    flex: 1,
    width: '100%'
  },
  fullScreen: {
    height: '100%',
    width: '100%'
  }
});

export default styles;
