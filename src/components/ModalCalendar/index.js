import React from 'react';
import { Modal, TouchableWithoutFeedback, View, Text } from 'react-native';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import noop from 'lodash/noop';
import { string, func, bool } from 'prop-types';
import styles from './styles';

const {
  modalContainer,
  confirmationModalWrapper,
  modalTextWrapper,
  blueNavBarText,
  typographySmall,
  confirmationModalShade,
  modalTextBorder,
  calendarContainer
} = styles;

const ModalCalendar = props => {
  const {
    modalVisible,
    hideModal,
    message,
    selectedDate,
    onDateSelect
  } = props;
  LocaleConfig.locales.es = {
    monthNames: [
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Agosto',
      'Septiembre',
      'Octubre',
      'Noviembre',
      'Diciembre'
    ],
    monthNamesShort: [
      'Ene.',
      'Feb.',
      'Marzo',
      'Abril',
      'Mayo',
      'Jun.',
      'Jul.',
      'Ago.',
      'Sept.',
      'Oct.',
      'Nov.',
      'Dic.'
    ],
    dayNames: [
      'Domingo',
      'Lunes',
      'Martes',
      'Miercoles',
      'Jueves',
      'Viernes',
      'Sábado'
    ],
    dayNamesShort: ['Do', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sa'],
    today: 'Hoy'
  };
  LocaleConfig.defaultLocale = 'es';
  return (
    <Modal
      transparent
      animationType="none"
      visible={modalVisible}
      supportedOrientations={['portrait']}
      onRequestClose={() => {}}
    >
      <View style={modalContainer}>
        <TouchableWithoutFeedback onPress={hideModal}>
          <View style={confirmationModalShade} />
        </TouchableWithoutFeedback>
        <View style={confirmationModalWrapper}>
          <View style={modalTextWrapper}>
            <View style={modalTextBorder}>
              <Text style={[blueNavBarText, typographySmall]} fontCase="bold">
                {message}
              </Text>
            </View>
          </View>
          <View style={calendarContainer}>
            <Calendar
              minDate={Date()}
              // Handler which gets executed on day press. Default = undefined
              onDayPress={day => {
                onDateSelect(day.dateString);
                hideModal();
              }}
              // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
              monthFormat="yyyy MMMM"
              theme={{
                selectedDayBackgroundColor: '#00adf5',
                selectedDayTextColor: '#ffffff'
              }}
              markedDates={{
                [selectedDate]: { selected: true }
              }}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

ModalCalendar.propTypes = {
  message: string,
  hideModal: func,
  modalVisible: bool,
  selectedDate: string,
  onDateSelect: func
};

ModalCalendar.defaultProps = {
  message: '',
  hideModal: noop,
  modalVisible: false,
  selectedDate: 'notYetSelected',
  onDateSelect: noop
};

export default ModalCalendar;
