import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

const {
  pVModalOptionContainer,
  darkGrayText,
  typographyMedium,
  pVModalOptionBorder,
  lightBlueText
} = styles;

const ModalPickerOption = props => {
  const textStyles = props.isSelected
    ? [typographyMedium, lightBlueText]
    : [typographyMedium, darkGrayText];
  return (
    <TouchableOpacity
      onPress={() => {
        props.onOptionPress(props.stateField, [
          props.label,
          props.value,
          props.index
        ]);
      }}
    >
      <View style={pVModalOptionContainer}>
        <View style={pVModalOptionBorder} />
        <Text style={textStyles}>{props.label}</Text>
      </View>
    </TouchableOpacity>
  );
};

ModalPickerOption.propTypes = {
  onOptionPress: PropTypes.func,
  label: PropTypes.string,
  value: PropTypes.string,
  index: PropTypes.number,
  isSelected: PropTypes.bool,
  stateField: PropTypes.string
};

ModalPickerOption.defaultProps = {
  label: '',
  onOptionPress: () => {},
  value: '',
  index: -1,
  isSelected: false,
  stateField: ''
};

export default ModalPickerOption;
