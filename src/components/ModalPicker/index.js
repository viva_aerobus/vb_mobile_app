import React, { Component } from 'react';
import {
  Modal,
  TouchableWithoutFeedback,
  View,
  ScrollView,
  Text
} from 'react-native';
import PropTypes from 'prop-types';
import ModalPickerOption from './ModalPickerOption';
import styles from './styles';

const {
  modalContainer,
  confirmationModalWrapper,
  modalTextWrapper,
  blueNavBarText,
  typographySmall,
  modalOptionsWrapper,
  confirmationModalShade,
  modalTextBorder
} = styles;

class ModalPicker extends Component {
  generateOptionsMarkup = () =>
    this.props.options.map((item, index) => (
      <ModalPickerOption
        isSelected={this.props.selectedIndex === index}
        onOptionPress={this.props.onOptionPress}
        label={item.label}
        value={item.value}
        index={item.index}
        stateField={this.props.stateField}
        key={item.label}
      />
    ));
  render() {
    const { modalVisible, hideModal } = this.props;
    return (
      <Modal
        transparent
        animationType="none"
        visible={modalVisible}
        supportedOrientations={['portrait']}
        onRequestClose={() => {}}
      >
        <View style={modalContainer}>
          <TouchableWithoutFeedback onPress={hideModal}>
            <View style={confirmationModalShade} />
          </TouchableWithoutFeedback>
          <View style={confirmationModalWrapper}>
            <View style={modalTextWrapper}>
              <View style={modalTextBorder}>
                <Text style={[blueNavBarText, typographySmall]} fontCase="bold">
                  {this.props.message}
                </Text>
              </View>
            </View>
            <ScrollView style={modalOptionsWrapper}>
              {this.generateOptionsMarkup()}
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

ModalPicker.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.string
    })
  ),
  message: PropTypes.string,
  hideModal: PropTypes.func,
  modalVisible: PropTypes.bool,
  selectedIndex: PropTypes.number,
  onOptionPress: PropTypes.func.isRequired,
  stateField: PropTypes.string
};

ModalPicker.defaultProps = {
  options: [],
  message: '',
  hideModal: () => {},
  modalVisible: false,
  selectedIndex: 0,
  stateField: ''
};

export default ModalPicker;
