import { StyleSheet, Dimensions } from 'react-native';
import {
  darkBlue,
  lightBlue,
  darkGray,
  divisionBarGray
} from '../../vb_data_layer/styles/colors';
import { scalingFactor } from '../../vb_data_layer/styles/constants';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default StyleSheet.create({
  typographySmall: {
    fontSize: (screenWidth / 28) * scalingFactor
  },
  typographyMedium: {
    fontSize: (screenWidth / 19) * scalingFactor
  },
  blueNavBarText: {
    color: darkBlue
  },
  lightBlueText: {
    color: lightBlue
  },
  darkGrayText: {
    color: darkGray
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  confirmationModalShade: {
    width: screenWidth,
    height: screenHeight * 0.65,
    backgroundColor: '#00000077'
  },
  confirmationModalWrapper: {
    width: screenWidth,
    height: screenHeight * 0.35,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: screenHeight * 0.03
  },
  modalTextWrapper: {
    width: screenWidth,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalTextBorder: {
    width: screenWidth * 0.85,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderBottomColor: darkBlue
  },
  modalOptionsWrapper: {
    width: screenWidth,
    height: screenHeight * 0.14
  },
  pVModalOptionContainer: {
    height: screenHeight * 0.05,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative'
  },
  pVModalOptionBorder: {
    height: screenHeight * 0.05,
    width: screenWidth * 0.6,
    borderBottomWidth: 1,
    borderBottomColor: divisionBarGray,
    position: 'absolute',
    zIndex: 0
  }
});
