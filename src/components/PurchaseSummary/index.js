import React from 'react';
import { Text, View } from 'react-native';
import { string, number, arrayOf, shape } from 'prop-types';
import { numberWithCommas } from '../../vb_data_layer/utils';
import styles from './styles';

const {
  formLabelTextSize,
  darkGrayText,
  flightNumberTextSize,
  lightBlueText,
  flightNumberWrapper,
  flightSummaryEntryWrapper,
  purchaseDetailsDivisionLine,
  purchaseSummaryFrame,
  purchaseSummaryContainer
} = styles;

const PurchaseSummary = props => {
  const generateServiceCharges = charges =>
    charges.map(item => (
      <View style={flightSummaryEntryWrapper}>
        <Text style={formLabelTextSize}>
          <Text style={darkGrayText}>{`${item.detail}`}</Text>
          <Text>{'  '}</Text>
          <Text style={lightBlueText}>
            {`$${numberWithCommas(item.amount)}`}
          </Text>
        </Text>
      </View>
    ));
  return (
    <View style={purchaseSummaryFrame}>
      <View style={purchaseSummaryContainer}>
        <View style={flightNumberWrapper}>
          <Text style={flightNumberTextSize}>
            <Text style={darkGrayText}>Total</Text>
            <Text>{'  '}</Text>
            <Text style={lightBlueText}>{`$${numberWithCommas(
              props.total
            )}`}</Text>
          </Text>
        </View>
        <View style={purchaseDetailsDivisionLine} />
        <View style={flightNumberWrapper}>
          <Text style={flightNumberTextSize}>
            <Text style={darkGrayText}>Subtotal</Text>
            <Text>{'  '}</Text>
            <Text style={lightBlueText}>{`$${numberWithCommas(
              props.subtotal
            )}`}</Text>
          </Text>
        </View>
        <View style={flightNumberWrapper}>
          <Text style={flightNumberTextSize}>
            <Text style={darkGrayText}>Impuestos totales: </Text>
            <Text>{'  '}</Text>
            <Text style={lightBlueText}>
              {`$${numberWithCommas(props.totalTax)}`}
            </Text>
          </Text>
        </View>
        <View style={flightNumberWrapper}>
          <Text style={flightNumberTextSize}>
            <Text style={darkGrayText}>Recargos: </Text>
            <Text>{'  '}</Text>
            <Text style={lightBlueText}>
              {`$${numberWithCommas(props.surcharges)}`}
            </Text>
          </Text>
        </View>
        <View style={purchaseDetailsDivisionLine} />
        <View style={flightNumberWrapper}>
          <Text style={flightNumberTextSize}>
            <Text style={darkGrayText}>Impuestos desglosados: </Text>
          </Text>
        </View>
        <View style={flightSummaryEntryWrapper}>
          <Text style={formLabelTextSize}>
            <Text style={darkGrayText}>Iva:</Text>
            <Text>{'  '}</Text>
            <Text style={lightBlueText}>
              {`$${numberWithCommas(props.ivaTotal)}`}
            </Text>
          </Text>
        </View>
        <View style={flightSummaryEntryWrapper}>
          <Text style={formLabelTextSize}>
            <Text style={darkGrayText}>
              Impuestos por prestación de servicio:
            </Text>
          </Text>
        </View>
        {generateServiceCharges(props.serviceCharges)}
      </View>
    </View>
  );
};

PurchaseSummary.propTypes = {
  total: number,
  subtotal: number,
  totalTax: number,
  ivaTotal: number,
  serviceCharges: arrayOf(
    shape({
      detail: string,
      amount: number
    })
  ),
  surcharges: number
};

PurchaseSummary.defaultProps = {
  total: 0,
  totalTax: 0,
  ivaTotal: 0,
  serviceCharges: [],
  subtotal: 0,
  surcharges: 0
};

export default PurchaseSummary;
