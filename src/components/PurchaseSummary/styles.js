import { StyleSheet, Dimensions } from 'react-native';

import { scalingFactor } from '../../vb_data_layer/styles/constants';

import {
  darkGray,
  mediumGray,
  lightBlue,
  darkBlue
} from '../../vb_data_layer/styles/colors';

const { width: screenWidth, height: screenHeight } = Dimensions.get('window');

const styles = StyleSheet.create({
  formLabelTextSize: {
    fontSize: (screenWidth / 23) * scalingFactor
  },
  typographyMedium: {
    fontSize: (screenWidth / 19) * scalingFactor
  },
  flightNumberTextSize: {
    fontSize: (screenWidth / 17) * scalingFactor
  },
  typographyExtraLarge: {
    fontSize: (screenWidth / 14) * scalingFactor
  },
  darkGrayText: {
    color: darkGray
  },
  whiteText: {
    color: '#FFF'
  },
  lightBlueText: {
    color: lightBlue
  },
  darkBlueText: {
    color: darkBlue
  },
  locationWrapper: {
    width: screenWidth * 0.35,
    height: screenHeight * 0.14,
    justifyContent: 'center'
  },
  purchaseSummaryContainer: {
    width: screenWidth * 0.9,
    minHeight: screenHeight * 0.18,
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#FFF',
    borderRadius: 17
  },
  flightNumberWrapper: {
    width: screenWidth * 0.9,
    height: screenHeight * 0.04,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.04
  },
  flightSummaryEntryWrapper: {
    width: screenWidth * 0.9,
    height: screenHeight * 0.035,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.04
  },
  flightListItemFrame: {
    width: screenWidth,
    minHeight: screenHeight * 0.18,
    justifyContent: 'center',
    alignItems: 'center'
  },
  fareItemColor0: {
    backgroundColor: 'rgba(255,250,232,1)'
  },
  fareItemColor1: {
    backgroundColor: 'rgba(255,244,209,1)'
  },
  fareItemColor2: {
    backgroundColor: 'rgba(255,239,186,1)'
  },
  purchaseDetailsDivisionLine: {
    width: screenWidth * 0.9,
    borderBottomColor: mediumGray,
    borderBottomWidth: 3
  },
  purchaseSummaryFrame: {
    width: screenWidth,
    height: screenHeight * 0.7,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default styles;
