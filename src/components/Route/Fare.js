import React from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { string, number, func, bool } from 'prop-types';
import noop from 'lodash/noop';
import { numberWithCommas } from '../../vb_data_layer/utils';
import styles from './styles';

const vivaFare = require('../../../assets/vbImages/vivaFare.png');
const vivaSuperFare = require('../../../assets/vbImages/vivaSuperFare.png');
const vivaMaxFare = require('../../../assets/vbImages/vivaMaxFare.png');

const fareIconsArray = [vivaFare, vivaSuperFare, vivaMaxFare];

const {
  formLabelTextSize,
  flightNumberTextSize,
  fareItemContainer,
  flightNumberWrapper,
  flightSummaryEntryWrapper,
  fareListItemFrame,
  fareGlobalWrapper,
  fareCheckboxContainer,
  fareIconContainer,
  fareTitleText,
  whiteText,
  lightGreenText,
  fareIconStyle,
  checkBoxFare,
  selectedFare
} = styles;

const Fare = props => {
  const fareItemColor = `fareItemColor${props.index % 3}`;
  return (
    <TouchableOpacity
      style={fareListItemFrame}
      onPress={() =>
        props.onFareSelected(props.routeIndex, props.flightIndex, props.index)
      }
    >
      <View style={fareGlobalWrapper}>
        <View style={[fareIconContainer, styles[fareItemColor]]}>
          <Image
            style={fareIconStyle}
            source={fareIconsArray[props.index % 3]}
          />
        </View>
        <View style={[fareItemContainer, styles[fareItemColor]]}>
          <View style={flightNumberWrapper}>
            <Text style={flightNumberTextSize}>
              <Text style={fareTitleText}>{props.name}</Text>
            </Text>
          </View>
          <View style={flightSummaryEntryWrapper}>
            <Text style={formLabelTextSize}>
              <Text style={whiteText}>Precio:</Text>
              <Text>{'  '}</Text>
              <Text style={lightGreenText}>{`$${numberWithCommas(
                props.price
              )}`}</Text>
            </Text>
          </View>
          <View style={flightSummaryEntryWrapper}>
            <Text style={formLabelTextSize}>
              <Text style={whiteText}>Precio con VivaClub:</Text>
              <Text>{'  '}</Text>
              <Text style={lightGreenText}>{`$${numberWithCommas(
                props.vivaClubPrice
              )}`}</Text>
            </Text>
          </View>
          <View style={flightSummaryEntryWrapper}>
            <Text style={formLabelTextSize}>
              <Text style={whiteText}>Asientos disponibles:</Text>
              <Text>{'  '}</Text>
              <Text style={lightGreenText}>{props.availabilityCount}</Text>
            </Text>
          </View>
        </View>
        <View style={[fareCheckboxContainer, styles[fareItemColor]]}>
          <View style={checkBoxFare}>
            {props.isSelected ? <View style={selectedFare} /> : null}
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

Fare.propTypes = {
  name: string,
  price: number,
  vivaClubPrice: number,
  availabilityCount: number,
  onFareSelected: func,
  index: number,
  flightIndex: number,
  routeIndex: number,
  isSelected: bool
};

Fare.defaultProps = {
  name: '',
  price: 0,
  availabilityCount: 0,
  vivaClubPrice: 0,
  onFareSelected: noop,
  index: 0,
  flightIndex: 0,
  routeIndex: null,
  isSelected: false
};

export default Fare;
