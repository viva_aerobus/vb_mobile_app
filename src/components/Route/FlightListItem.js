import React from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import moment from 'moment';
import noop from 'lodash/noop';
import { string, number, bool, shape, arrayOf, func } from 'prop-types';
import Fare from './Fare';
import { numberWithCommas } from '../../vb_data_layer/utils';
import styles from './styles';

const smallPlane = require('../../../assets/vbImages/smallPlane.png');

const {
  formLabelTextSize,
  flightNumberTextSize,
  flightItemContainer,
  flightNumberWrapper,
  flightSummaryEntryWrapper,
  flightListItemFrame,
  whiteBackground,
  flightDataWrapper,
  flightIconContainer,
  flightTextGray,
  flightTextGreen,
  entryGray,
  entryGreen,
  flightIconStyle
} = styles;

const FlightListItem = props => {
  const generateFaresList = fares =>
    fares.map((item, index) => (
      <Fare
        name={item.name}
        price={item.price}
        availabilityCount={item.availabilityCount}
        vivaClubPrice={item.vivaClubPrice}
        index={index}
        flightIndex={props.flightIndex}
        onFareSelected={props.onFareSelected}
        routeIndex={props.routeIndex}
        isSelected={props.isSelected && props.clickedFareItem === index}
      />
    ));
  return (
    <TouchableOpacity
      style={flightListItemFrame}
      onPress={props.onFlightListItemClick}
    >
      <View style={[flightItemContainer, whiteBackground]}>
        <View style={flightIconContainer}>
          <Image style={flightIconStyle} source={smallPlane} />
        </View>
        <View style={flightDataWrapper}>
          <View style={flightNumberWrapper}>
            <Text style={flightNumberTextSize}>
              <Text style={flightTextGray}>Vuelo</Text>
              <Text>{'  '}</Text>
              <Text style={flightTextGreen}>{props.flightNumber}</Text>
            </Text>
          </View>
          <View style={flightSummaryEntryWrapper}>
            <Text style={formLabelTextSize}>
              <Text style={entryGray}>Salida:</Text>
              <Text>{'  '}</Text>
              <Text style={entryGreen}>
                {moment(props.departureDate, 'YYYY-MM-DDThh:mm:ss').format(
                  'DD.MM.YYYY \u2022 HH:mm'
                )}
              </Text>
            </Text>
          </View>
          <View style={flightSummaryEntryWrapper}>
            <Text style={formLabelTextSize}>
              <Text style={entryGray}>Llegada:</Text>
              <Text>{'  '}</Text>
              <Text style={entryGreen}>
                {moment(props.arrivalDate, 'YYYY-MM-DDThh:mm:ss').format(
                  'DD.MM.YYYY \u2022 HH:mm'
                )}
              </Text>
            </Text>
          </View>
          <View style={flightSummaryEntryWrapper}>
            <Text style={formLabelTextSize}>
              <Text style={entryGray}>Tiene escalas:</Text>
              <Text>{'  '}</Text>
              <Text style={entryGreen}>
                {props.hasConnections ? 'Sí' : 'No'}
              </Text>
            </Text>
          </View>
          <View style={flightSummaryEntryWrapper}>
            <Text style={formLabelTextSize}>
              <Text style={entryGray}>Precio más bajo:</Text>
              <Text>{'  '}</Text>
              <Text style={flightTextGreen}>{`$${numberWithCommas(
                props.lowestPrice
              )}`}</Text>
            </Text>
          </View>
        </View>
      </View>
      {props.isSelected ? generateFaresList(props.fares) : null}
    </TouchableOpacity>
  );
};

FlightListItem.propTypes = {
  flightNumber: string,
  arrivalDate: string,
  departureDate: string,
  lowestPrice: number,
  hasConnections: bool,
  fares: arrayOf(shape({})),
  onFlightListItemClick: func,
  isSelected: bool,
  flightIndex: number,
  onFareSelected: func,
  routeIndex: number,
  clickedFareItem: number
};

FlightListItem.defaultProps = {
  flightNumber: '',
  arrivalDate: '',
  departureDate: '',
  lowestPrice: 0,
  hasConnections: false,
  fares: [],
  onFlightListItemClick: noop,
  isSelected: false,
  flightIndex: 0,
  onFareSelected: noop,
  routeIndex: null,
  clickedFareItem: null
};

export default FlightListItem;
