import React from 'react';
import { Text, View, Image } from 'react-native';
import { string } from 'prop-types';
import styles from './styles';

const {
  routeHeaderContainer,
  locationWrapper,
  locationTitle,
  locationContent,
  routeHeaderFrame,
  locationCodeText,
  locationAnnotationText,
  rightArrowWrapper,
  arrowRightStyle
} = styles;

const arrowRight = require('../../../assets/vbImages/arrowRight.png');

const RouteHeader = props => {
  return (
    <View style={routeHeaderFrame}>
      <View style={routeHeaderContainer}>
        <View style={locationWrapper}>
          <View style={locationContent}>
            <Text style={locationCodeText}>{props.from}</Text>
          </View>
          <View style={locationTitle}>
            <Text style={locationAnnotationText}>ORIGEN</Text>
          </View>
        </View>
        <View style={rightArrowWrapper}>
          <Image style={arrowRightStyle} source={arrowRight} />
        </View>
        <View style={locationWrapper}>
          <View style={locationContent}>
            <Text style={locationCodeText}>{props.to}</Text>
          </View>
          <View style={locationTitle}>
            <Text style={locationAnnotationText}>DESTINO</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

RouteHeader.propTypes = {
  from: string,
  to: string
};

RouteHeader.defaultProps = {
  from: '',
  to: ''
};

export default RouteHeader;
