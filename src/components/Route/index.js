import React from 'react';
import { arrayOf, string, shape, func, number } from 'prop-types';
import noop from 'lodash/noop';
import { View, Text } from 'react-native';
import RouteHeader from './RouteHeader';
import FlightListItem from './FlightListItem';
import styles from './styles';

const {
  noFlightsMessageContainer,
  flightNumberWrapper,
  formLabelTextSize,
  darkGrayText,
  flightListItemFrame
} = styles;

const Route = props => {
  const flightListItems = props.flights.map((item, index) => (
    <FlightListItem
      flightNumber={item.flightNumber}
      arrivalDate={item.arrivalDate}
      departureDate={item.departureDate}
      lowestPrice={item.lowestPrice}
      hasConnections={item.segments.length > 1}
      isSelected={index === props.selectedFlight}
      onFlightListItemClick={() =>
        props.onFlightListItemClick(props.routeIndex, index)
      }
      flightIndex={index}
      fares={item.fares}
      onFareSelected={props.onFareSelected}
      routeIndex={props.routeIndex}
      clickedFareItem={props.clickedFareItem}
    />
  ));
  return (
    <View>
      <RouteHeader from={props.from} to={props.to} />
      {props.flights.length !== 0 ? (
        flightListItems
      ) : (
        <View style={flightListItemFrame}>
          <View style={noFlightsMessageContainer}>
            <View style={flightNumberWrapper}>
              <Text style={formLabelTextSize}>
                <Text style={darkGrayText}>
                  No se encontraron vuelos a este destino
                </Text>
              </Text>
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

Route.propTypes = {
  from: string,
  to: string,
  flights: arrayOf(shape({})),
  onFlightListItemClick: func,
  selectedFlight: number,
  onFareSelected: func,
  routeIndex: number,
  clickedFareItem: number
};

Route.defaultProps = {
  from: '',
  to: '',
  flights: [],
  onFlightListItemClick: noop,
  selectedFlight: null,
  onFareSelected: noop,
  routeIndex: null,
  clickedFareItem: null
};

export default Route;
