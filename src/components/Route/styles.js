import { StyleSheet, Dimensions } from 'react-native';

import { scalingFactor } from '../../vb_data_layer/styles/constants';

import {
  darkGray,
  mediumGray,
  lightBlue,
  darkBlue,
  vbGreenActive,
  vbLightGreenText,
  vbDarkGreenText,
  vbGreenText,
  vbFareItem1,
  vbFareItem2
} from '../../vb_data_layer/styles/colors';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  formLabelTextSize: {
    fontSize: (screenWidth / 23) * scalingFactor
  },
  typographyMedium: {
    fontSize: (screenWidth / 19) * scalingFactor
  },
  flightNumberTextSize: {
    fontSize: (screenWidth / 17) * scalingFactor
  },
  typographyExtraLarge: {
    fontSize: (screenWidth / 14) * scalingFactor
  },
  darkGrayText: {
    color: darkGray
  },
  whiteText: {
    color: '#FFF'
  },
  lightBlueText: {
    color: lightBlue
  },
  darkBlueText: {
    color: darkBlue
  },
  lightGreenText: {
    color: vbLightGreenText
  },
  routeHeaderContainer: {
    width: screenWidth * 0.85,
    height: screenHeight * 0.12,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  whiteBackground: {
    backgroundColor: '#FFF'
  },
  lightGrayBackround: {
    backgroundColor: 'rgba(219,219,219,1)'
  },
  locationWrapper: {
    width: screenWidth * 0.35,
    height: screenHeight * 0.12,
    justifyContent: 'center'
  },
  locationTitle: {
    width: screenWidth * 0.35,
    height: screenHeight * 0.05,
    justifyContent: 'center',
    alignItems: 'center'
  },
  locationContent: {
    width: screenWidth * 0.35,
    height: screenHeight * 0.07,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  routeHeaderFrame: {
    width: screenWidth,
    height: screenHeight * 0.12,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: screenHeight * 0.02,
    backgroundColor: vbGreenActive
  },
  flightNumberWrapper: {
    width: screenWidth * 0.9,
    height: screenHeight * 0.035,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.02
  },
  flightSummaryEntryWrapper: {
    width: screenWidth * 0.9,
    height: screenHeight * 0.03,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: screenWidth * 0.02
  },
  fareItemColor0: {
    backgroundColor: vbGreenActive
  },
  fareItemColor1: {
    backgroundColor: vbFareItem1
  },
  fareItemColor2: {
    backgroundColor: vbFareItem2
  },
  fareListItemFrame: {
    width: screenWidth,
    height: screenHeight * 0.14,
    justifyContent: 'center',
    alignItems: 'center'
  },
  fareCheckboxContainer: {
    width: screenWidth * 0.15,
    height: screenHeight * 0.16,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 8
  },
  selectedMark: {
    borderRadius: 7,
    borderWidth: 1,
    borderColor: mediumGray,
    backgroundColor: darkBlue,
    width: screenHeight * 0.04,
    height: screenHeight * 0.04
  },
  counterActionable: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: mediumGray,
    backgroundColor: '#FFF',
    width: screenHeight * 0.06,
    height: screenHeight * 0.06,
    justifyContent: 'center',
    alignItems: 'center'
  },
  noFlightsMessageContainer: {
    width: screenWidth * 0.9,
    minHeight: screenHeight * 0.14,
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: '#FFF'
  },
  locationCodeText: {
    fontWeight: '400',
    fontSize: (screenWidth / 11) * scalingFactor,
    color: '#FFF'
  },
  locationAnnotationText: {
    fontWeight: '400',
    fontSize: (screenWidth / 19) * scalingFactor,
    color: vbLightGreenText
  },
  rightArrowWrapper: {
    width: screenWidth * 0.1,
    height: screenHeight * 0.12,
    justifyContent: 'center'
  },
  arrowRightStyle: {
    height: screenHeight * 0.07,
    width: screenWidth * 0.15,
    resizeMode: 'contain'
  },
  flightListItemFrame: {
    width: screenWidth,
    minHeight: screenHeight * 0.22,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: screenHeight * 0.02
  },
  flightItemContainer: {
    width: screenWidth * 0.9,
    height: screenHeight * 0.21,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 8,
    flexDirection: 'row'
  },
  flightDataWrapper: {
    width: screenWidth * 0.76,
    height: screenHeight * 0.22,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 8
  },
  flightIconContainer: {
    width: screenWidth * 0.1,
    height: screenHeight * 0.22,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: screenHeight * 0.02
  },
  flightTextGray: {
    fontWeight: '700',
    color: darkGray
  },
  flightTextGreen: {
    fontWeight: '600',
    color: vbDarkGreenText
  },
  entryGray: {
    fontWeight: '600',
    color: darkGray
  },
  entryGreen: {
    fontWeight: '500',
    color: vbGreenText
  },
  flightIconStyle: {
    height: screenHeight * 0.07,
    width: screenWidth * 0.09,
    resizeMode: 'contain'
  },
  fareGlobalWrapper: {
    width: screenWidth * 0.9,
    height: screenHeight * 0.16,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  fareItemContainer: {
    width: screenWidth * 0.65,
    height: screenHeight * 0.16,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingTop: screenHeight * 0.01
  },
  fareIconContainer: {
    width: screenWidth * 0.1,
    height: screenHeight * 0.16,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: screenHeight * 0.02,
    borderTopLeftRadius: 8
  },
  fareTitleText: {
    fontWeight: '600',
    color: '#FFF'
  },
  fareIconStyle: {
    height: screenHeight * 0.07,
    width: screenWidth * 0.07,
    resizeMode: 'contain'
  },
  checkBoxFare: {
    borderRadius: screenHeight * 0.03,
    borderWidth: 2,
    borderColor: '#FFF',
    width: screenHeight * 0.03,
    height: screenHeight * 0.03,
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectedFare: {
    borderRadius: screenHeight * 0.015,
    backgroundColor: '#FFF',
    width: screenHeight * 0.015,
    height: screenHeight * 0.015
  }
});

export default styles;
