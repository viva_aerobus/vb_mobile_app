import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import noop from 'lodash/noop';
import { string, number, arrayOf, func } from 'prop-types';

import styles from './styles';

const {
  tabsContainer,
  singleTabContainer,
  tabLabelWrapper,
  tabLabelText,
  tabBottomBorder,
  bottomBorderActive,
  labelTextActive
} = styles;

const Tabs = props => {
  const generateTabs = labels =>
    labels.map((label, index) => {
      const labelText =
        index === props.selectedIndex
          ? [tabLabelText, labelTextActive]
          : tabLabelText;
      const bottomBorder =
        index === props.selectedIndex
          ? [tabBottomBorder, bottomBorderActive]
          : tabBottomBorder;
      return (
        <TouchableOpacity
          style={singleTabContainer}
          onPress={() => props.onTabSelection(index)}
        >
          <View style={tabLabelWrapper}>
            <Text style={labelText}>{label}</Text>
          </View>
          <View style={bottomBorder} />
        </TouchableOpacity>
      );
    });
  return <View style={tabsContainer}>{generateTabs(props.labelsList)}</View>;
};

Tabs.propTypes = {
  labelsList: arrayOf(string),
  selectedIndex: number,
  onTabSelection: func
};

Tabs.defaultProps = {
  labelsList: [],
  selectedIndex: 0,
  onTabSelection: noop
};

export default Tabs;
