import { StyleSheet, Dimensions } from 'react-native';

import { scalingFactor } from '../../vb_data_layer/styles/constants';

import {
  darkGray,
  lightBlue,
  darkBlue,
  vbGreenShaded,
  vbGreenActive
} from '../../vb_data_layer/styles/colors';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  formLabelTextSize: {
    fontSize: (screenWidth / 23) * scalingFactor
  },
  typographyMedium: {
    fontSize: (screenWidth / 19) * scalingFactor
  },
  darkGrayText: {
    color: darkGray
  },
  whiteText: {
    color: '#FFF'
  },
  lightBlueText: {
    color: lightBlue
  },
  darkBlueText: {
    color: darkBlue
  },
  tabsContainer: {
    width: screenWidth,
    height: screenHeight * 0.05,
    flexDirection: 'row',
    alignItems: 'center'
  },
  singleTabContainer: {
    width: screenWidth * 0.33,
    height: screenHeight * 0.05,
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabLabelText: {
    fontWeight: '600',
    fontSize: (screenWidth / 24) * scalingFactor,
    color: vbGreenShaded
  },
  tabLabelWrapper: {
    width: screenWidth * 0.45,
    height: screenHeight * 0.045,
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabBottomBorder: {
    width: screenWidth * 0.26,
    height: screenHeight * 0.005,
    borderBottomColor: 'rgba(255,255,255,0)',
    borderBottomWidth: 3
  },
  bottomBorderActive: {
    borderBottomColor: vbGreenActive
  },
  labelTextActive: {
    color: '#FFF'
  }
});

export default styles;
