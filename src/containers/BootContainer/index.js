import React from 'react';
import { View, Text, Image } from 'react-native';
import { shape, func } from 'prop-types';
import { Asset } from 'expo-asset';
import styles from './styles';

const { container, footerContainer, logoStyle, footer } = styles;
const splashScreen = require('../../../assets/vivaSplash.png');

class BootContainer extends React.Component {
  async componentDidMount() {
    await Asset.loadAsync(splashScreen);
    return this.props.navigation.navigate('FlightSelectionForm');
  }
  render() {
    return (
      <View style={container}>
        <Image source={splashScreen} style={logoStyle} />
        <View style={footerContainer}>
          <Text style={footer}>Viva Aerobus ® 2020</Text>
        </View>
      </View>
    );
  }
}

BootContainer.propTypes = {
  navigation: shape({
    navigate: func
  }).isRequired
};

BootContainer.defaultProps = {};

export default BootContainer;
