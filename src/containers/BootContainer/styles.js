import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
const smaller = width * 0.03;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'rgb(155, 155, 155)'
  },
  logoStyle: {
    width,
    height,
    resizeMode: 'contain'
  },
  footer: {
    color: '#FFFFFF',
    fontSize: smaller,
    marginBottom: height * 0.047
  },
  footerContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default styles;
