import React from 'react';
import { View, StatusBar, Modal, Text } from 'react-native';
import { WebView } from 'react-native-webview';
import { WaveIndicator } from 'react-native-indicators';
import { func, shape, objectOf, string } from 'prop-types';
import get from 'lodash/get';
import styles from './styles';

class CheckoutContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showResultModal: false
    };
  }
  handleResponse = async message => {
    const parsedMessage = JSON.parse(message.nativeEvent.data);
    this.setState({
      showResultModal: true
    });
    const code = get(parsedMessage, 'code', null);
    if (code) {
      await this.props.actions.exchangePaypalAccessTokens(code);
    }
  };
  render() {
    const { navigation, paypal } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <Modal
          animationType="fade"
          transparent={false}
          visible={this.state.showResultModal}
        >
          <Text>Modal up</Text>
        </Modal>
        <View style={styles.contentsWrapper}>
          {navigation.isFocused() &&
          (paypal === null || paypal.status === 'connecting') ? (
            <WaveIndicator
              size={100}
              waveFactor={0.6}
              color="rgba(200,200,200,0.8)"
              count={5}
            />
          ) : (
            <WebView
              originWhitelist={['*']}
              useWebKit
              style={styles.webViewStyle}
              source={{
                uri: 'http://localhost:3000/'
              }}
              injectedJavaScript="window.ReactNativeWebView.postMessage(document.getElementById('message').innerHTML)"
              onMessage={msg => this.handleResponse(msg)}
            />
          )}
        </View>
      </View>
    );
  }
}

CheckoutContainer.navigationOptions = {
  header: null
};

CheckoutContainer.propTypes = {
  navigation: shape({
    func
  }).isRequired,
  actions: objectOf(func),
  paypal: shape({
    approvalUrl: string,
    code: string,
    status: string
  })
};

CheckoutContainer.defaultProps = {
  actions: {},
  paypal: {}
};

/*
function mapStateToProps(state) {
  return {
    ...state.payments,
    ...state.purchaseOrders,
    cartItems: state.cart.items
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        ...paymentsActions,
        resetCart: cartReset
      },
      dispatch
    )
  };
}
*/

export default CheckoutContainer;

/*
export default withTranslation()(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PaymentMethodDetails)
);
*/
