import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    width,
    height,
    position: 'relative'
  },
  contentsWrapper: {
    position: 'absolute',
    top: height * 0.05,
    width,
    height: height - height * 0.08
  },
  webViewStyle: {
    flex: 1
  }
});

export default styles;
