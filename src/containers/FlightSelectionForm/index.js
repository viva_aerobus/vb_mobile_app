import React, { Component } from 'react';
import { ScrollView, AsyncStorage, Alert, View } from 'react-native';
import { shape, func, string, objectOf, number } from 'prop-types';
import get from 'lodash/get';
import { connect } from 'react-redux';
import noop from 'lodash/noop';
import { bindActionCreators } from 'redux';
import FlightForm from '../../components/FlightForm';
import FullScreenModalPicker from '../../components/FullScreenModalPicker';
import FullScreenCalendarModal from '../../components/FullScreenCalendarModal';
import FullScreenGeneralModal from '../../components/FullScreenGeneralModal';
import CounterButton from '../../components/FlightForm/CounterButton';
import Layout from '../../components/Layout';
import * as formActions from '../../vb_data_layer/actions/formData';
import {
  airportsSelector,
  currenciesSelector
} from '../../vb_data_layer/utils/selectors';
import styles from './styles';

const infant = require('../../../assets/vbImages/infant.png');
const adult = require('../../../assets/vbImages/adult.png');
const child = require('../../../assets/vbImages/child.png');

class FlightSelectionForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      origin: '',
      destiny: '',
      currency: 'COP',
      departureDate: '',
      returnDate: '',
      modalMessage: 'Please select: ',
      modalVisible: false,
      pickerOptions: [],
      currentModalStateField: '',
      adults: 1,
      children: 0,
      infants: 0,
      selectedTripOptionIndex: 0,
      modalCalendarVisible: false,
      currentCalendarField: '',
      calendarMessage: '',
      modalPassengersVisible: false
    };
  }
  componentDidMount() {
    this.props.actions.getFormData();
  }
  createSearchApi = async () => {
    const passengers = [
      {
        count: this.state.adults,
        code: 'ADT'
      },
      {
        count: this.state.children,
        code: 'CHD'
      },
      {
        count: this.state.infants,
        code: 'INFT'
      }
    ];
    const searchFlights = {
      passengers,
      currencyCode: this.state.currency[0],
      routes: [
        {
          fromAirport: this.state.origin[1],
          startDate: this.state.departureDate,
          endDate: this.state.departureDate,
          toAirport: this.state.destiny[1]
        }
      ],
      isManageSearch: false
    };
    const routesReturn = {
      fromAirport: this.state.destiny[1],
      startDate: this.state.returnDate,
      endDate: this.state.returnDate,
      toAirport: this.state.origin[1]
    };
    if (this.state.selectedTripOptionIndex !== 0) {
      searchFlights.routes.push(routesReturn);
    }
    await AsyncStorage.setItem('searchFlights', JSON.stringify(searchFlights));
  };

  onChangeText = (key, value) => {
    this.setState({ [key]: value });
  };
  onDelete = key => {
    this.setState({ [key]: '' });
  };
  onOptionPress = (key, item) => {
    this.setState({
      modalVisible: false,
      [key]: item
    });
  };
  setModalPickerOptions = (
    options,
    message,
    comment,
    currentStateField,
    commentIcon = null
  ) => {
    this.setState({
      pickerOptions: options,
      modalMessage: message,
      modalComment: comment,
      currentModalStateField: currentStateField,
      modalCommentIcon: commentIcon
    });
  };
  setModalCalendarOptions = (message, currentCalendarField) => {
    this.setState({
      calendarMessage: message,
      currentCalendarField
    });
  };
  getSelectedIndex = () => {
    if (this.state.pickerOptions.length === 0) {
      return -1;
    }
    const selectedValue = this.state[this.state.currentModalStateField];
    return selectedValue[2];
  };
  onDateSelect = date => {
    this.setState({
      [this.state.currentCalendarField]: date
    });
  };
  cancelEdit = () => {
    this.props.navigation.goBack();
  };
  validateFormFields = () => {
    const {
      destiny,
      origin,
      currency,
      departureDate,
      returnDate,
      selectedTripOptionIndex
    } = this.state;
    const roundTripCondition =
      selectedTripOptionIndex === 1 ? returnDate === '' : false;
    if (
      destiny === '' ||
      origin === '' ||
      currency === '' ||
      departureDate === '' ||
      roundTripCondition
    ) {
      Alert.alert(
        'Información faltante',
        'Por favor llena todos los campos del formulario',
        [{ text: 'Aceptar', onPress: noop }],
        { cancelable: false }
      );
      return false;
    }
    return true;
  };
  submit = () => {
    if (this.validateFormFields()) {
      this.createSearchApi();
      this.props.navigation.navigate('RoutesList');
    }
  };
  modifyCounter = (key, operator) => {
    this.setState(prevState => {
      const newCount =
        operator === '+' ? prevState[key] + 1 : prevState[key] - 1;
      const possitiveCount = newCount > 0 ? newCount : 0;
      const finalCount =
        key === 'adults' && possitiveCount < 1 ? 1 : possitiveCount;
      return { [key]: finalCount };
    });
  };
  updateModalStatus = () => {
    this.setState(state => ({ modalVisible: !state.modalVisible }));
  };
  updateModalPassengersStatus = () => {
    this.setState(state => ({
      modalPassengersVisible: !state.modalPassengersVisible
    }));
  };
  updateModalCalendarStatus = () => {
    this.setState(state => ({
      modalCalendarVisible: !state.modalCalendarVisible
    }));
  };
  onOptionSelection = index => {
    this.setState({ selectedTripOptionIndex: index });
  };
  updateAmountType = amountType => this.setState({ amountType });
  render() {
    const { formCase } = this.props;
    const createCase = formCase !== 'edit';
    const placeholders = {
      origin: 'Selecciona un Origen',
      destiny: 'Selecciona un Destino',
      currency: 'Selecciona una Divisa',
      departureDate: 'Selecciona una Fecha',
      returnDate: 'Selecciona una Fecha'
    };
    const values = {
      origin: this.state.origin,
      destiny: this.state.destiny,
      currency: this.state.currency,
      departureDate: this.state.departureDate,
      returnDate: this.state.returnDate,
      adults: this.state.adults,
      children: this.state.children,
      infants: this.state.infants,
      selectedTripOptionIndex: this.state.selectedTripOptionIndex
    };
    const tripOptions = ['SENCILLO', 'IDA Y VUELTA', 'MULTI-CITY'];
    return (
      <Layout process={this.props.process} customBackground>
        <ScrollView>
          <FlightForm
            placeholders={placeholders}
            onSubmit={() => this.submit(createCase)}
            onCancel={this.cancelEdit}
            setModalPickerOptions={this.setModalPickerOptions}
            updateModalStatus={this.updateModalStatus}
            updateModalCalendarStatus={this.updateModalCalendarStatus}
            updateModalPassengersStatus={this.updateModalPassengersStatus}
            setModalCalendarOptions={this.setModalCalendarOptions}
            airportsArray={this.props.airportsListing}
            values={values}
            currenciesArray={this.props.currenciesListing}
            modifyCounter={this.modifyCounter}
            tripOptions={tripOptions}
            isOneWayTicket={this.state.selectedTripOptionIndex === 0}
            onOptionSelection={this.onOptionSelection}
            formButtonsLabels={{
              cancel: 'Cancelar',
              submit: 'Buscar'
            }}
          />
        </ScrollView>
        <FullScreenModalPicker
          headerLabel={this.state.modalMessage}
          comment={this.state.modalComment}
          onOptionPress={this.onOptionPress}
          options={this.state.pickerOptions}
          modalVisible={this.state.modalVisible}
          hideModal={this.updateModalStatus}
          stateField={this.state.currentModalStateField}
          selectedIndex={this.getSelectedIndex()}
          modalCommentIcon={this.state.modalCommentIcon}
          withSearchBar
        />
        <FullScreenCalendarModal
          headerLabel={this.state.calendarMessage}
          hideModal={this.updateModalCalendarStatus}
          modalVisible={this.state.modalCalendarVisible}
          onDateSelect={this.onDateSelect}
          selectedDate={this.state[this.state.currentCalendarField]}
        />
        <FullScreenGeneralModal
          headerLabel="Selecciona tus pasajeros"
          hideModal={this.updateModalPassengersStatus}
          modalVisible={this.state.modalPassengersVisible}
        >
          <View style={styles.counterButtonsContainer}>
            <CounterButton
              label="Adultos"
              currentCount={values.adults}
              formField="adults"
              modifyCounter={this.modifyCounter}
              labelComplement="Más de 12 años"
              labelIcon={adult}
            />
            <CounterButton
              label="Niños"
              currentCount={values.children}
              formField="children"
              modifyCounter={this.modifyCounter}
              labelComplement="Entre 2 y 11 años"
              labelIcon={child}
            />
            <CounterButton
              label="Infantes"
              currentCount={values.infants}
              formField="infants"
              modifyCounter={this.modifyCounter}
              labelComplement="Menores de 2 años"
              labelIcon={infant}
            />
          </View>
        </FullScreenGeneralModal>
      </Layout>
    );
  }
}

FlightSelectionForm.propTypes = {
  actions: objectOf(func),
  navigation: shape({
    navigate: func,
    goBack: func
  }),
  formCase: string,
  airportsListing: shape({
    label: string,
    value: string,
    index: number
  }),
  currenciesListing: shape({
    label: string,
    value: string,
    index: number
  }),
  process: string
};

FlightSelectionForm.defaultProps = {
  actions: {},
  navigation: {},
  formCase: '',
  currenciesListing: {},
  airportsListing: {},
  process: null
};

function mapStateToProps(state) {
  return {
    airportsListing: airportsSelector(get(state, 'formData.airportsArray', [])),
    currenciesListing: currenciesSelector(
      get(state, 'formData.currenciesArray', [])
    ),
    process: get(state, 'formData.process', null)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, formActions), dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FlightSelectionForm);
