import { StyleSheet, Dimensions } from 'react-native';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  counterButtonsContainer: {
    height: screenHeight * 0.4,
    width: screenWidth,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: screenHeight * 0.03
  }
});

export default styles;
