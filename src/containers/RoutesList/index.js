import React, { Component } from 'react';
import { ScrollView, AsyncStorage, Alert, View } from 'react-native';
import { shape, func, objectOf, string, arrayOf } from 'prop-types';
import get from 'lodash/get';
import noop from 'lodash/noop';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Layout from '../../components/Layout';
import Route from '../../components/Route';
import FormButtons from '../../components/FlightForm/FormButtons';
import styles from './styles';

import * as flightsActions from '../../vb_data_layer/actions/flightsData';

class RoutesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departureRoute: {
        clickedFlightListItem: null,
        clickedFareItem: null
      },
      returnRoute: {
        clickedFlightListItem: null,
        clickedFareItem: null
      },
      departureFare: null,
      returnFare: null
    };
  }
  componentDidMount() {
    this.props.actions.getFlightsData();
  }
  produceDecodedPassengers = (passengerKey, quantity) => {
    let i;
    const resultArray = [];
    for (i = 0; i < quantity; i += 1) {
      resultArray.push({ type: passengerKey });
    }
    return resultArray;
  };
  createTripData = async () => {
    const searchFlights = await AsyncStorage.getItem('searchFlights');
    const parsedSearchFlights = JSON.parse(searchFlights);
    const { passengers } = parsedSearchFlights;
    const decodedPassengers = [
      ...this.produceDecodedPassengers(passengers[0].code, passengers[0].count),
      ...this.produceDecodedPassengers(
        passengers[1].code,
        passengers[1].count,
        ...this.produceDecodedPassengers(
          passengers[2].code,
          passengers[2].count
        )
      )
    ];
    const journeys = [
      {
        fareAvailabilityKey: this.state.departureFare.availabilityKey,
        price: this.state.departureFare.price,
        journeyKey: this.props.routes[0].flights[
          this.state.departureRoute.clickedFlightListItem
        ].key
      }
    ];
    if (this.state.returnFare !== null) {
      journeys.push({
        fareAvailabilityKey: this.state.returnFare.availabilityKey,
        price: this.state.returnFare.price,
        journeyKey: this.props.routes[1].flights[
          this.state.returnRoute.clickedFlightListItem
        ].key
      });
    }
    const tripData = {
      journeys,
      passengers: decodedPassengers
    };
    await AsyncStorage.setItem('tripData', JSON.stringify(tripData));
  };
  onFlightListItemClick = (routeIndex, index) => {
    const route = routeIndex === 0 ? 'departureRoute' : 'returnRoute';
    this.setState(prevState => {
      const newFlightValue =
        index === prevState[route].clickedFlightListItem ? null : index;
      const newFareValue =
        index !== prevState[route].clickedFlightListItem
          ? null
          : prevState[route].clickedFlightListItem;
      return {
        [route]: {
          clickedFlightListItem: newFlightValue,
          clickedFareItem: newFareValue
        }
      };
    });
  };
  onCancel = () => {
    this.props.navigation.navigate('FlightSelectionForm');
  };
  routesBasicValidation = () => {
    const isRoundTrip = this.props.routes.length > 1;
    const validCondition = isRoundTrip
      ? this.state.departureFare !== null && this.state.returnFare !== null
      : this.state.departureFare !== null;
    if (!validCondition) {
      const alertTitle = 'Tarifa no seleccionada';
      const alertMessage = isRoundTrip
        ? 'Es necesario seleccionar una tarifa de ida y una de regreso'
        : 'Es necesario seleccionar una tarifa para tu viaje';
      Alert.alert(
        alertTitle,
        alertMessage,
        [{ text: 'Aceptar', onPress: noop }],
        { cancelable: false }
      );
      return false;
    }
    return true;
  };
  onSubmit = () => {
    if (this.routesBasicValidation()) {
      this.createTripData();
      this.props.navigation.navigate('TripData');
    }
  };
  onFareSelected = (routeIndex, flightIndex, index) => {
    const { routes } = this.props;
    const routeKey = routeIndex === 1 ? 'returnRoute' : 'departureRoute';
    const fareKey = routeIndex === 1 ? 'returnFare' : 'departureFare';
    this.setState({
      [fareKey]: routes[routeIndex].flights[flightIndex].fares[index],
      [routeKey]: {
        clickedFareItem: index,
        clickedFlightListItem: flightIndex
      }
    });
  };
  render() {
    return (
      <Layout process={this.props.process}>
        <View style={styles.screenColoredMargin} />
        <ScrollView style={{ backgroundColor: 'rgba(255,255,255,0)' }}>
          {this.props.routes.map((route, index) => {
            const routeName = index === 0 ? 'departureRoute' : 'returnRoute';
            return (
              <Route
                from={route.from.name}
                to={route.to.name}
                flights={route.flights}
                onFlightListItemClick={this.onFlightListItemClick}
                selectedFlight={this.state[routeName].clickedFlightListItem}
                routeIndex={index}
                onFareSelected={this.onFareSelected}
                clickedFareItem={this.state[routeName].clickedFareItem}
              />
            );
          })}
          <FormButtons
            formButtonsLabels={{
              cancel: 'Atrás',
              submit: 'Cotizar'
            }}
            onSubmit={this.onSubmit}
            onCancel={this.onCancel}
          />
        </ScrollView>
      </Layout>
    );
  }
}

RoutesList.propTypes = {
  actions: objectOf(func),
  routes: arrayOf(shape({})),
  process: string,
  navigation: shape({
    navigate: func,
    goBack: func
  })
};

RoutesList.defaultProps = {
  actions: {},
  navigation: {},
  routes: [],
  process: null
};

function mapStateToProps(state) {
  return {
    routes: get(state, 'flightsData.searchResponse', []),
    process: get(state, 'flightsData.process', null)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, flightsActions), dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoutesList);
