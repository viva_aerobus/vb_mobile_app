import { StyleSheet, Dimensions } from 'react-native';
import { vbGreenActive } from '../../vb_data_layer/styles/colors';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  screenColoredMargin: {
    height: screenHeight * 0.05,
    width: screenWidth,
    backgroundColor: vbGreenActive,
    position: 'absolute',
    top: 0
  }
});

export default styles;
