import React, { Component } from 'react';
import { ScrollView, Alert } from 'react-native';
import { shape, func, objectOf, string, arrayOf } from 'prop-types';
import get from 'lodash/get';
import noop from 'lodash/noop';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Layout from '../../components/Layout';
import PurchaseSummary from '../../components/PurchaseSummary';
import FormButtons from '../../components/FlightForm/FormButtons';

import * as tripActions from '../../vb_data_layer/actions/tripData';

class TripData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this.props.actions.getTripData();
  }
  onCancel = () => {
    this.props.navigation.navigate('RoutesList');
  };
  routesBasicValidation = () => {
    const isRoundTrip = this.props.routes.length > 1;
    const validCondition = isRoundTrip
      ? this.state.departureFare !== null && this.state.returnFare !== null
      : this.state.departureFare !== null;
    if (!validCondition) {
      const alertTitle = 'Tarifa no seleccionada';
      const alertMessage = isRoundTrip
        ? 'Es necesario seleccionar una tarifa de ida y una de regreso'
        : 'Es necesario seleccionar una tarifa para tu viaje';
      Alert.alert(
        alertTitle,
        alertMessage,
        [{ text: 'Aceptar', onPress: noop }],
        { cancelable: false }
      );
      return false;
    }
    return true;
  };
  onSubmit = () => {
    console.log('trip data confirmed');
  };
  render() {
    console.log('properties in TripData: ', this.props);
    const { process, tripData } = this.props;
    return (
      <Layout process={process}>
        <ScrollView style={{ backgroundColor: 'rgba(255,255,255,0)' }}>
          <PurchaseSummary
            total={get(tripData, 'basket.total', 0)}
            totalTax={get(tripData, 'basket.totalTax', 0)}
            ivaTotal={get(tripData, 'basket.iva.totalAmount', 0)}
            serviceCharges={get(tripData, 'basket.taxes.serviceCharges', [])}
            subtotal={get(tripData, 'basket.fares.totalAmount', 0)}
            surcharges={get(tripData, 'basket.surcharges.totalAmount', 0)}
          />
          <FormButtons
            formButtonsLabels={{
              cancel: 'Atrás',
              submit: 'Comprar'
            }}
            onSubmit={this.onSubmit}
            onCancel={this.onCancel}
          />
        </ScrollView>
      </Layout>
    );
  }
}

TripData.propTypes = {
  actions: objectOf(func),
  routes: arrayOf(shape({})),
  process: string,
  navigation: shape({
    navigate: func,
    goBack: func
  })
};

TripData.defaultProps = {
  actions: {},
  navigation: {},
  routes: [],
  process: null
};

function mapStateToProps(state) {
  return {
    tripData: get(state, 'tripData.createTripArray', {}),
    process: get(state, 'tripData.process', null)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, tripActions), dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripData);
