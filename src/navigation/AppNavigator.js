import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import BootContainer from '../containers/BootContainer';
import CheckoutContainer from '../containers/CheckoutContainer';
import FlightSelectionForm from '../containers/FlightSelectionForm';
import RoutesList from '../containers/RoutesList';
import TripData from '../containers/TripData';

export default createAppContainer(
  createSwitchNavigator(
    {
      BootContainer,
      FlightSelectionForm,
      RoutesList,
      CheckoutContainer,
      TripData
    },
    {
      initialRouteName: 'BootContainer'
    }
  )
);
