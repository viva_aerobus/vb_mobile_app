// import { refresh } from 'services/auth';
// import { authRefreshRequest, authRefreshResponse } from 'actions/auth';

// let authPromise = null;

export default function callAPIMiddleware({ getState }) {
  return next => action => {
    const {
      types,
      callAPI,
      shouldCallAPI = () => true,
      payload = {}
      // shouldRefresh = true
    } = action;

    if (!types) {
      return next(action);
    }

    if (
      !Array.isArray(types) ||
      types.length !== 2 ||
      !types.every(type => typeof type === 'string')
    ) {
      throw new Error('APIMiddleware: Expected an array of two string types.');
    }

    const [requestType, responseType] = types;
    const currentState = getState();
    /*  session auth/expiration code goes here

    const { accessToken, refreshToken, exp } = currentState.auth;
    const currentTime = Math.floor(Date.now() / 1000);
    const needRefresh = exp - currentTime <= 0;

    if (shouldRefresh && accessToken && needRefresh) {
      if (authPromise) {
        return authPromise
          .then(res =>
            callAPI({
              ...currentState,
              auth: {
                ...currentState.auth,
                ...res
              }
            })
          )
          .then(response => next({ payload, response, type: responseType }))
          .catch(error => console.log(error)); // [WIP]
      }

      next(authRefreshRequest());

      authPromise = refresh(refreshToken);

      return authPromise
        .then(res => {
          next(authRefreshResponse(res));
          return callAPI({
            ...currentState,
            auth: {
              ...currentState.auth,
              accessToken: res.accessToken
            }
          });
        })
        .then(response => {
          authPromise = null;
          return next({ payload, response, type: responseType });
        })
        .catch(error => {
          authPromise = null;
          console.log(error);
        }); // [WIP]
    }

    */

    if (!shouldCallAPI(currentState)) {
      return 0;
    }

    next({ payload, type: requestType });

    return callAPI(currentState)
      .then(response => next({ payload, response, type: responseType }))
      .catch(error => console.log(error)); // [WIP]
  };
}
