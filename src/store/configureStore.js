import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import APIMiddleware from './APIMiddleware';

const middlewares = composeWithDevTools({})(
  applyMiddleware(thunk, APIMiddleware)
);

export default function configureStore(rootReducer, initialState = {}) {
  const mergedState = { ...initialState };
  const store = createStore(rootReducer, mergedState, middlewares);
  return store;
}
